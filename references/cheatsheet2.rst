.. References documentation master file, created by
   sphinx-quickstart on Fri Nov  8 00:10:21 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ReStructuredText CheatSheet
======================================

Contents:

.. toctree::
   :maxdepth: 2
   
   rst_cheatsheet
    



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

