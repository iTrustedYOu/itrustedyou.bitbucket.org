Python essential reference
==========================

Ovde drzim stvari koje su mi bile nove u ovoj knjizi,
tj. nisu mi bas na povrsini sto se tice pajtona. Kad
budem gledala ovo dovoljno puta, videcu sta mogu da izbacim...
Importing modules as different names is useful for stuff
like this:
``
if format == 'xml':
    import xmlreader as reader
elif format == 'csv':
    import csvreader as reader
data = reader.read_data(filename)
``
Underneath the covers, a module object is a layer over a dic-
tionary that is used to hold the contents of the module namespace.This dictionary is
available as the _ _dict_ _ of a module, and whenever you look up or change a value in
a module, you’re working with this dictionary.

You can find a dictionary con-
taining all currently loaded modules in the variable sys.modules.

``
from spam import (foo,
                  bar,
                  Spam)
# module: spam.py
_ _all_ _ = [ 'bar', 'Spam' ]
# Names I will export with from spam import *
``
Thus, the global namespace for a function is
always the module in which the function was defined, not the namespace into which a
function is imported and called.

Vazno je zapamtiti da kad uradimo from nesto import a, mi mozemo
staviti imenu a neku drugu vrednost i izgubiti originalno a, tj. ne
moze
nam biti kao neka konstanta. Ako hocemo globalnu variablu iz drugog
modula
koja ce zadrzati svoju vrednost - onda koristimo nesto.a .

A relative import more clearly states your
intentions.

``
from ..Primitives import lines
``

Relative imports can only be specified using the from module import symbol
form of the import statement.Thus, statements such as import ..Primitives.lines
or import .lines are a syntax error


``
# Graphics/_ _init_ _.py
from . import Primitive, Graph2d, Graph3d
# Graphics/Primitive/_ _init_ _.py
from . import lines, fill, text, ...
``
Now the import Graphics statement imports all the submodules and makes them
available using their fully qualified names. Again, it is important to stress that a package
relative import should be used as shown. If you use a simple statement such as import
module, standard library modules may be loaded instead.

**Pajtonova len metoda nam dopusta da definisemo logicki model
objekta. znaci to je nesto sto ima duzinu. i to je kul!!** Zato se ja
uvek pitam kako tako neki list-like objekti, kako na njih moze da se
primeni len(..), a to je verovatno za to sto su redefinisali ovo.


