Ono sto sam naucila uceci django o njemu, webp i pajtonu
========================================================
manage.py je *wrapper* oko ``django-admin.py``

::

  In addition, manage.py is automatically created in each Django
  project. manage.py is a thin wrapper around django-admin.py that
  takes care of two things for you before delegating to django-admin.py:

    It puts your project’s package on sys.path.
    It sets the DJANGO_SETTINGS_MODULE environment variable so that it
    points to your project’s settings.py file.


I stvarno izgleda ko da je wrapper, znaci on uradi neke potrebne
stvari pre nego sto svom 'coru' delegira, a i vidim ima neke dodatne
specificne opcije za sesije i za staticke fajlove (sta god to za
staticke bilo??)


Znaci manage.py ce exportovati env variablu pa ce django gledati u
settingse bas u nasoj ``settings.py`` datoteci. A ono sto je kul za
ovu datoteku jeste posto je pajton datoteka pa se moze izvrsavati
pajton kod dinamicki i znaci settingsi se dinamicki namestati. **Ne
znam jos uvek gde ce to biti potrebno doduse**.

Automatic reloading of runserver

The development server automatically reloads Python code for each
request as needed. You don’t need to restart the server for code
changes to take effect. However, some actions like adding files or
compiling translation files don’t trigger a restart, so you’ll have to
restart the server in these cases.

hmm, ``syncdb`` napravi tabele za bazu podataka, i onda one sada
postoje to su inicijalne tabele, i potrebne su za minimalne aplikacije
iz ``INSTALLED_APPS`` settinga
::
   Automatic reloading of runserver

   The development server automatically reloads Python
   code for each request as needed. 
   You don’t need to restart the server for code changes to take
   effect. 
   However, some actions like adding files or 
   compiling translation files don’t trigger a restart,
   so you’ll have to restart the server in these cases.


Kul, znaci kad uradimo ``manage.py syncdb`` naprave se tabele za
instalirane aplikacije iz ``settings.py`` i njih mogi da gledam iz
jako kul firefox **SQLiteManager** iz kog mogu videti i sastav
napravljenih tabela (sadrzaj imaju django_conent_type,
auth_permissions, auth_user - jedino superuser registrovan). Ostale su
prazne!

Ovo sa pravljenjem modela je jako kul!

Znaci mi samo napravimo aplikaciju preko
``python manage.py startapp polls``
i onda ce nam on napraviti models.py gde cemo mi
napisati nase modele (nasledjuju odredjenu klasu)
i onda ce svaki attribut koji mu napisemo
biti ustvari kolona u tabeli koja ce se zvati po
klasi, i samo ce ta kolona biti neki
django objekat koji odgovara tipu podatka
iz sqlite-a u ovom slucaju. Preko komande
``python manage.py sql polls`` cemo
dobitii ``CREATE_TABLE`` komandu za nase
tabele, koje mozemo ili sami izvrsiti, ili ce se
izvrsiti kad se uradi ``python manage.py syncdb`` tj.
tada ce se izvrsiti sve sto vidimo kad uradimo komandu
``python manage.py sqlall polls`` za sve aplikacije u 
``INSTALLED_APPS`` settingu i staviit u nas originalnu
sqlite bazu podataka (datoteka u mysite folderu). Samo
pravi tabele koje vec ne postoje! 

Joj jako dobra integrisanost djanga sa bazom podataka
znaci mi postupamo sa tom torkom koju napravimo kao 
sa obicnim objektom, samo sto moramo da pozovemo ``save()``
metodu da bi ga ovaj upisao u bazu podataka, i dao mu automatski
id. a ovo sve ostalo je isto!! Takodje mozemo pristupiti svim
torkama odredjene tabele tako sto uradimo
``ImeObjektaTabele.objects.all()`` << Jako kul! 
A koristimo ::
     def __unicode__(self):
        return self.question_text

kako bismo imali lepsu reprezentaciju pri izvrsavanju prethodne
komande!

Django ima veoma bogate api za database lookup koji je potpuno
kontrolisan keyword argumentima. Ovo je moguce, posto pajton ima
opciju ``**kwargs`` koja dozvoljava da se evaluaisu imena keyword
argumentata u runtime-a, znaci samo je potrebno da konstruisemo
string koji cemo gledati u tom dictu, ustvari, samo ce verovatno
pogledati sta ima u ``**kwargs`` i onda iz toga ce generisati
SQL query ili nesto. Znaci sistem ide na sledeci nacin - mislim da se
samo koristi `_` umesto `.` Znaci ako hocemo da izdvojimo sve izbore
nekog pitanja (oba su tabele, svako pitanje moze 'imati' vise izbora),
po tome da li su ove godine izdati, radimo sledece 
```q.choices_set.filter(pub_date__year=current_year)```

Zanimljivo kako je uspeo sve da ih ispoveze. Pa mozemo npr. filtrirati
i izbore po tome sa kojim pitanjem su povezani, i sve sto zamislimo.
Pogledaj ovo detaljnije ako ti bude trebalo! 

Kul je ovaj admin, znaci kada registrujemo objekat tu, on napravi kao
neki gui putem kojim cemo mi manipulisati tom tabelom u sqlite bazi.
Mozemo da editujemo, pa cemo dobiti odgovarajuce html+javascript
componente za zeljen tip obelezja u bazi podataka. Sve promene se
odmah vide u bazi podataka, i prate se u tabeli ``django_admin_log``


Part 3
------

Ovde prica o view-ovima, uglavnom obavezni parametar za svaki view je
``HttpRequest`` a obavezna povratna vrednost je ``HttpResponse`` ili
da bacimo neki exception. Uglavnom, zgodno je sto se url-ovi
specificiraju pomocu regexa, tako da mozemo da koristimo koji god
format url-a hocemo, a da izvlacimo parametre iz toga, named parametre
mozemo da prosledjujemo pomocu named-group opcije regexa, tj. opcije
da se zada ime grupe koju zelimo da izdvojimo. Ovo mi se i svidja.
Uglavnom, svaki app moze imati svoju url sekciju, drzimo je npr. u
folderu aplikacije, pa onda includeujemo u glavni urls kod regexa koji
hocemo da matchuje tu aplikaciju. Onda je on vec isparsirao taj deo
pa gleda taj urls od aplikacije od dalje, znaci ``r'^$`` ce biti home
stranica aplikacije, sve ostalo ce biti dodato na to. 

::
   
  When Django finds a regular expression match, Django calls the
  specified view function,
  with an HttpRequest object as the first argument and any “captured”
  values from the regular
  expression as other arguments. If the regex uses simple captures,
  values are  passed as positional arguments; if it uses named
  captures,
  values are passed as keyword arguments.

'name' polje u url() patternu nam sluzi za identifikaciju laku.

::
   
   Note that the regular expressions for the include() functions don’t
   have
   a $ (end-of-string match character) but rather a trailing slash. 
   Whenever Django encounters include(), it chops off whatever part 
   of the URL matched up to that point and sends the remaining string 
   to the included URLconf for further processing.

::
   
  Django’s TEMPLATE_LOADERS setting contains a list of callables that
  know how to import templates 
  from various sources. One of the defaults is
  django.template.loaders.app_directories.
  Loader which looks for a “templates” subdirectory in each of the
  INSTALLED_APPS

::
   
   Because of how the app_directories template loader works as
   described above, 
   you can refer to this template within Django simply as
   polls/index.html.

::
   
   Now we might be able to get away with putting our templates
   directly 
   in polls/templates (rather than creating another polls
   subdirectory), 
   but it would actually be a bad idea. Django will choose the first 
   template it finds whose name matches, and if you had a template
   with 
   the same name in a different application, Django would be unable 
   to distinguish between them. We need to be able to point Django at 
   the right one, and the easiest way to ensure this is by namespacing 
   them. That is, by putting those templates inside another directory 
   named for the application itself.


`vise o tome kako template-i rade
<https://docs.djangoproject.com/en/1.6/topics/templates/>`_.

::
   

   Technically, when the template system encounters a dot, it tries the following lookups, in this order:

    -Dictionary lookup
    -Attribute lookup
    -Method call
    -List-index lookup

::

   If you use a variable that doesn’t exist, the template system will
   insert the value of the TEMPLATE_STRING_IF_INVALID setting,
   which is set to '' (the empty string) by default.

   Note that “bar” in a template expression like {{ foo.bar }}
   will be interpreted as a literal string and not using the value of the variable “bar”, if one exists in the template context.



`Trebalo bi da proucis ovo, da znas sta sve ima django builtin
<https://docs.djangoproject.com/en/1.6/ref/templates/builtins/>`_.


**Hm, nisam sigurna za csrf, mi ga saljemo, ali ga nigde ne
hvatamo???**

Part 4
------

::

   These views represent a common case of basic Web development: getting
   data from the database 
   according to a parameter passed in the URL, loading a template and
   returning the rendered template. 
   Because this is so common, Django provides a shortcut, called the
   “generic views” system.


**Nije mi jasno uopste, kako mi kad uradimo get na neki ``django
view`` mi dobijemo u contextu ono sto bi po meni trebalo samo u
templejtu da postoji. Znaci kad se renderuje nas templejt, mhm, mi
specificiramo neki context, ali on ne bi trebalo da se prosledjuje
u response-u, ne?? Ako s vec sve resilo u toj metodi, mi bismo trebali
da vracamo samo httpresponse, nije mi jasno?????**

Mozda ovo??::

  setup_test_environment() installs a template renderer which will
  allow us to examine some additional attributes on responses such
  as response.context 

Izgleda da ce za svaki test se praviti posebna test tabela, tj za 
svaku funkciju iz testa, posto tako pretpostavlja kod.

::

  And so on. In effect, we are using the tests to tell a story of 
  admin input and user experience on the site, and checking that 
  at every state and for every new change in the state of the system,
  the expected results are published.
